/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID INICIO = 2658298240U;
        static const AkUniqueID PLAY_CUBOSEQUENCE = 3970786644U;
        static const AkUniqueID PLAY_PELOTARANDOM = 3257296292U;
        static const AkUniqueID PLAY_SPHERE_1 = 3086544757U;
        static const AkUniqueID PLAY_SPHERE_3 = 3086544759U;
        static const AkUniqueID PLAY_SPHERE_4 = 3086544752U;
        static const AkUniqueID PLAY_SPHERE_5 = 3086544753U;
        static const AkUniqueID PLAY_SPHERE_6 = 3086544754U;
        static const AkUniqueID UNMUTE_SP_A2_TRIPLE_LASER_L1_01 = 2374567018U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace MUSICREGION
        {
            static const AkUniqueID GROUP = 239549482U;

            namespace STATE
            {
                static const AkUniqueID DEFAULTGENERICA = 3621849380U;
                static const AkUniqueID DOMO = 1911402762U;
                static const AkUniqueID LABERINTO = 556669445U;
                static const AkUniqueID NONE = 748895195U;
            } // namespace STATE
        } // namespace MUSICREGION

    } // namespace STATES

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MAIN = 3161908922U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMBISONICS = 2903397179U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
